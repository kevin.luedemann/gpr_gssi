import h5py

def dump(filename,header,data):
    if filename[-4:] == "hdf5":
        f = h5py.File(filename,"w")
    elif filename[-4:] == "h5":
        f = h5py.File(filename,"w")
    else:
        f = h5py.File(filename+".hdf5","w")
    dset = f.create_dataset(header["filename"], data.shape,
                                                chunks=True,
                                                compression="gzip",
                                                compression_opts=9,
                                                data=data)
    for key in header:
        if key != "cdt" and key != "mdt":
            dset.attrs[key] = header[key]
    f.close()

def export(f,header,data):
    dset = f.create_dataset(header["filename"], data.shape,
                                                chunks=True,
                                                compression="gzip",
                                                compression_opts=9,
                                                data=data)
    for key in header:
        if key != "cdt" and key != "mdt":
            dset.attrs[key] = header[key]

def get_handle(filename):
    if filename[-4:] == "hdf5":
        f = h5py.File(filename,"a")
    elif filename[-4:] == "h5":
        f = h5py.File(filename,"a")
    else:
        f = h5py.File(filename+".hdf5","w")
    return f

def get_handle_ro(filename):
    if filename[-4:] == "hdf5":
        f = h5py.File(filename,"r")
    elif filename[-4:] == "h5":
        f = h5py.File(filename,"r")
    else:
        f = h5py.File(filename+".hdf5","r")
    return f
