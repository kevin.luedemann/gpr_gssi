from setuptools import setup

setup(name='gpr_gssi',
      version='0.1',
      description='Reader for GPR data in gssi file (.dzt) format',
      url='https://gitlab.gwdg.de/kevin.luedemann/gpr_gssi.git',
      author='Kevin Luedemann',
      author_email='kevin.luedemann@stud.uni-goettingen.de',
      packages=['gpr_gssi'],
      zip_safe=False)

