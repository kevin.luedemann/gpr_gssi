import numpy as np
import h5py
import gpr_gssi as dzt
from glob import glob
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mayavi import mlab

if __name__ == "__main__":
    f_h5        = dzt.output.hdf5.get_handle_ro(glob("*.hdf5")[0])
    keys        = list(f_h5.keys())
    data        = np.array(f_h5["cube"][...])
    data_global = np.array(f_h5["cube_global"][...])

    it,dimx,dimy = data.shape
    meters = f_h5[keys[0]].attrs.get("meters")
    range_ns = f_h5[keys[0]].attrs.get("range")
    X,Y,Z = np.mgrid[   0.:it,
                        0.:dimx,
                        0.:dimy
                        ]

    mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1))
    src = mlab.pipeline.scalar_field(X,Y,Z,data_global)
    #mlab.pipeline.iso_surface(src, contours=[data_global.min()+0.2*data_global.ptp(), ], opacity=0.1)
    mlab.pipeline.iso_surface(src, contours=[data_global.max()-0.2*data_global.ptp(), ],)
    surf = mlab.pipeline.image_plane_widget(src,
                                plane_orientation='x_axes',
                                slice_index=2
                            )
    mlab.axes(surf)

    mlab.show()
