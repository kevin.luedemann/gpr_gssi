import numpy as np
from glob import glob
import h5py
import sys,os
import gpr_gssi as dzt
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from skimage import exposure

def make_plots(header,data):
    #In numpy arrays the axis are flipped
    dimx,dimy = data.shape
    X,Y = np.mgrid[ slice(0.,header["meters"]   ,dimy*1j),
                    slice(0.,header["range"]    ,dimx*1j)]

    #Enhance the global contrast
    data_global = exposure.equalize_hist(data)

    #plot the reflections first original and enhanced
    fig1,ax1    = plt.subplots()
    ax1.pcolor(X,Y,data.T,
                 #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                 cmap="inferno")
    ax1.invert_yaxis()
    ax1.set_xlabel("distance/m")
    ax1.set_ylabel("depth/ns")
    ax1.set_title("original")

    fig2,ax2    = plt.subplots()
    ax2.pcolor(X,Y,data_global.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax2.invert_yaxis()
    ax2.set_xlabel("distance/m")
    ax2.set_ylabel("depth/ns")
    ax2.set_title("global contrast")

    #Show the wiggle plot
    fig3,ax3    = plt.subplots()
    ax3.plot(data.T[int(dimy/2)],np.arange(dimx),label="original")
    ax3.invert_yaxis()
    ax3.set_ylabel("depth/m")

    return (fig1,ax1),(fig2,ax2),(fig3,ax3)

if __name__ == "__main__":
    
    #Load the data for a file provided by CMD line
    header, data = dzt.gssi.readgssi(sys.argv[1], antfreq=200, stack=1)
    for key in sorted(header):
        print(key, header[key])

    #plotting
    make_plots(header,data)
    plt.show()

    #Ask for the beginning of the actual data cutting the part of the wave
    #entering the ground and the first reflection of the antenna.
    cut_hight = int(input("Cut hight:"))

    #Remove the cut part from the data
    dimx,dimy = data.shape
    data = data[cut_hight:]
    cut_time = np.arange(0.,header["range"],1./dimx)[cut_hight]

    #plotting again
    make_plots(header,data)
    plt.show()
