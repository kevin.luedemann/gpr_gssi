import numpy as np
from glob import glob
import h5py
import sys,os
import gpr_gssi as dzt
from skimage import exposure

def read_data(files,antfreq=200):
    results                = {}
    for fi in files:
        header, data    = dzt.gssi.readgssi(fi,
                                            antfreq=antfreq,
                                            stack=1)
        results.update({header["filename"]:{"header":header,"data":data}})
    print(results.keys())
    return results

if __name__ == "__main__":
    #Gather all the files in the folder
    files   = sorted(glob("*.DZT"))
    print(files)

    #read in all the files and data into a dict
    res     = read_data(files)

    #Collect all the shapes of the data
    keys    = list(res.keys())
    shapes = []
    for key in keys:
        shapes.append(np.array(res[key]["data"]).shape)
    shapes = np.array(shapes)
    print(shapes)

    #Find the longest dataset and pad all others to the same length
    max_lines   = np.max(shapes[:,1])
    print(max_lines)

    cube        = np.array(res[keys[0]]["data"])
    cube        = np.array(list(map(dzt.zero.shift,cube.T))).T
    le          = max_lines-cube.shape[1]
    cube        = np.pad(   cube,
                            [(0,0),(int(le/2),int(le/2))],
                            mode="constant").T

    #The result is saved as a cube of data
    cube        = np.expand_dims(cube,axis=0)
    cube_global = np.copy(exposure.equalize_hist(cube))
    for key in keys[1:]:
        cu          = np.array(res[key]["data"])
        cu          = np.array(list(map(dzt.zero.shift,cu.T))).T
        le          = max_lines-cu.shape[1]
        cu          = np.pad(   cu,
                                [(0,0),(int(le/2),int(le/2))],
                                mode="constant").T
        cube        = np.append(cube,
                                [cu],
                                axis=0)
        cube_global = np.append(cube_global,
                                [exposure.equalize_hist(cu)],
                                axis=0)

    #create the HDF5 file handle
    f_h5    = dzt.output.hdf5.get_handle(files[0].split("_")[0])

    #Sve the results in the HDF5 file as datasets
    dset1   = f_h5.create_dataset("cube",       cube.shape,
                                                chunks=True,
                                                compression="gzip",
                                                compression_opts=6,
                                                data=cube)
    dset2   = f_h5.create_dataset("cube_global",cube_global.shape,
                                                chunks=True,
                                                compression="gzip",
                                                compression_opts=6,
                                                data=cube_global)
    #Add important data to attributes
    for he in res[keys[0]]["header"]:
        print(he,res[keys[0]]["header"][he])
        if  he != "cdt" and \
            he != "mdt":
            dset1.attrs.create(he,res[keys[0]]["header"][he])
            dset2.attrs.create(he,res[keys[0]]["header"][he])

    f_h5.close()

