# GPR GSSI reader

This is a small API for getting data out of GSSI's .DZT files from the ground penetrating radar (gpr) used at the institute for [Geophysics](http://www.uni-goettingen.de/de/538888.html>) in Göttingen.
The original script and implementation was taken from [Ian Nesbitt's Github page](https://github.com/iannesbitt/readgssi).
Some of the comments in the text might still be from him.

This API sole purpose is to extract the header and data parts of the .DZT file and save it in a HDF5 file format.
This can further be used to perform some simple tasks like zero correction and plotting.

# Install

It is highly recommended to use conda for installing this package as well as the requirements.
A version of for example [miniconda](https://docs.conda.io/projects/miniconda/en/latest/) can be used.
It can be required to add the *conda-forge* channels to get the correct packages

    conda config --add channels conda-forge

To this end, set up a new environment and install python3.11:

    conda create --name gpr python=3.11

Activate it after installing

    conda activate gpr

The environment is ready for installing the required packages.

## Requirements

All requirements can be installed using conda with the command 

    conda install

The list of package is:

    numpy
    scipy
    matplotlib
    scikit-image
    pytz
    vtk
    hdf5
    h5py
    pyqt
    bitstruct
    numpydoc
    sphinx
    mayavi

The install command can look like this

    conda install numpy scipy matplotlib scikit-image pytz vtk hdf5 h5py pyqt bitstruct numpydoc sphinx mayavi

## Installing the package itself

This package can be installed by using pip.
A good practice is to use the linked version of installing using this command in the base directory of the repository

    pip install -e .

# Example scripts

Some example scripts can be found in the *demo* folder.
